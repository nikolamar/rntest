// @flow weak

import React from 'react'
import { Entypo } from '@expo/vector-icons'

const employeeIcon = {
  tabBarIcon: ({ focused }) => (
    <Entypo
      name="user"
      size={32}
      color={ focused ? "yellow" : "white" }
    />
  )
}

export default employeeIcon