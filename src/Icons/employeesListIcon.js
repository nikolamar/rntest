// @flow weak

import React from 'react'
import { Entypo } from '@expo/vector-icons'

const employeesListIcon = {
  tabBarIcon: ({ focused }) => (
    <Entypo
      name="users"
      size={32}
      color={ focused ? "yellow" : "white" }
    />
  )
}

export default employeesListIcon