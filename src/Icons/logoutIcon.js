// @flow weak

import React from 'react'
import { TouchableWithoutFeedback } from 'react-native'
import { NavigationActions } from 'react-navigation'
import { MaterialCommunityIcons } from '@expo/vector-icons'
import { Colors } from '../Styles'
import { resetAction } from '../Actions'

const logoutIcon = navigation => ({
  headerRight: (
    <TouchableWithoutFeedback
      onPress={() => navigation.dispatch(resetAction("LoginRoute"))}
    >
      <MaterialCommunityIcons
        style={{ paddingRight: 20 }}
        name="logout"
        size={30}
        color={Colors.Content}
      />
    </TouchableWithoutFeedback>
  )
})

export default logoutIcon