// @flow weak

class EmployeeModel {
  id
  employer_id
  job_term
  role
  full_description
  key_skills
  experience
  language
  rate
  rate_type
  masked_rate
  tronc
  living_wage
  multiple
  address
  location_town
  location_city
  postcode
  location_country
  latitude
  longitude
  comments
  posted
  status
  updated
  published
  expired
  first_name
  company
  company_type
  employer_img
  employer_img_v
  company_img
  company_img_v
  total
  dates

  constructor({ id, employer_id, job_term, role, full_description, key_skills, experience, language, rate, rate_type, masked_rate, tronc, living_wage, multiple, address, location_town, location_city, postcode, location_country, latitude, longitude, comments, posted, status, updated, published, expired, first_name, company, company_type, employer_img, employer_img_v, company_img, company_img_v, total, dates }) {
    this.id = id
    this.employer_id = employer_id
    this.job_term = job_term
    this.role = role
    this.full_description = full_description
    this.key_skills = key_skills
    this.experience = experience
    this.language = language
    this.rate = rate
    this.rate_type = rate_type
    this.masked_rate = masked_rate
    this.tronc = tronc
    this.living_wage = living_wage
    this.multiple = multiple
    this.address = address
    this.location_town = location_town
    this.location_city = location_city
    this.postcode = postcode
    this.location_country = location_country
    this.latitude = latitude
    this.longitude = longitude
    this.comments = comments
    this.posted = posted
    this.status = status
    this.updated = updated
    this.published = published
    this.expired = expired
    this.first_name = first_name
    this.company = company
    this.company_type = company_type
    this.employer_img = employer_img
    this.employer_img_v = employer_img_v
    this.company_img = company_img
    this.company_img_v = company_img_v
    this.total = total
    this.dates = dates
  }
}

export default EmployeeModel