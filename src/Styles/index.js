// @flow weak

import { StyleSheet } from 'react-native'

export const Colors = {
  Primary: "#03A9F4",
  Secondary: "#CED0CE",
  Selected: "#FFEB3B",
  Title: "#FFFFFF",
  Content: "#FFFFFF",
  Dark: "#000000",
}

export const Styles = StyleSheet.create({
  input: {
    marginTop: 15,
    marginLeft: 15,
    marginRight: 15,
    padding: 20,
    height: 60,
    fontSize: 14,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: Colors.Primary,
    opacity: 0.85,
  },
  loginButton: {
    marginTop: 10
  }
})