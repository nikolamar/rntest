// @flow weak

import React, { Component } from 'react'
import { View, Text, AsyncStorage, Animated, Dimensions } from 'react-native'
import Animation from 'lottie-react-native'
import { Colors } from '../Styles'
import { resetAction } from '../Actions'

class Loading extends Component {
  state = {
    loadingProgress: new Animated.Value(0)
  }
  componentWillMount() {
    this.tokenCheck()
  }
  componentDidMount() {
    Animated.loop(
      Animated.timing(this.state.loadingProgress, {
        toValue: 1,
        duration: 2000,
      })
    ).start()
  }
  tokenCheck = () => {
    // let user see authenticating
    // even if this action is so fast
    setTimeout(() => {
      AsyncStorage.getItem("token", (error, result) => {
        if (result) {
          this.props.navigation.dispatch(resetAction("TabsRoute"))
        } else {
          this.props.navigation.dispatch(resetAction("LoginRoute"))
        }
      })
      .catch(e => {
        alert(`Enable App access to SD: ${error}`)
      })
    }, 2000)
  }
  render() {
    return(
      <View
        style={{
          flex:1,
          flexDirection:'row',
          alignItems:'center',
          justifyContent:'center',
          backgroundColor: Colors.Primary
        }}
      >
        <Text style={{ position: "absolute", fontSize: 25, color: "white" }}>Authenticating...</Text>
        <View>
          <Animation
            style={{
              width: Dimensions.get("window").width,
              height: Dimensions.get("window").width,
            }}
            source={require('../Animations/loader_ring.json')}
            progress={this.state.loadingProgress}
          />
        </View>
      </View>
    )
  }
}

export default Loading