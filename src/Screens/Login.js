// @flow weak

import React, { Component } from 'react'
import { TouchableWithoutFeedback, View, Text, TextInput, Dimensions, ActivityIndicator, AsyncStorage } from 'react-native'
import { Button, FormValidationMessage } from 'react-native-elements'
import { Colors, Styles } from '../Styles'
import dismissKeyboard from 'dismissKeyboard'
import { Entypo } from '@expo/vector-icons'
import { resetAction } from '../Actions'

class Login extends Component {
  state = {
    email: "",
    pass: "",
    loading: false,
    submitted: false,
    landscape: Dimensions.get("window").width > Dimensions.get("window").height ? true : false
  }
  componentWillMount() {
    // could use fancy pancy async await :) but
    AsyncStorage.multiGet(["pass", "email"], (error, stores) => {
      const pass = stores[0][1]
      const email = stores[1][1]
      if (pass && email) {
        this.setState({ 
          pass: JSON.parse(pass),
          email: JSON.parse(email)
        })
      }
    })
    .catch(e => {
      alert(`Enable App access to SD: ${error}`)
    })
  }
  handleSubmit = () => {
    if (this.state.email.length && this.state.pass.length && this.validateEmail(this.state.email)) {
      dismissKeyboard()
      this.setState({ loading: true })

      const body = {
        method: "POST",
        headers: {
          "Accept": "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          "grant_type": "client_credentials",
          "client_id": this.state.email,
          "client_secret": this.state.pass
        })
      }
      // could use fancy pancy async await :) but
      fetch("https://test.inploi.me/token", body)      
        .then(res => res.json())
        .then(res => {
          if (!res.error) {
            AsyncStorage.multiSet([
              ["token", JSON.stringify(res.access_token)],
              ["email", JSON.stringify(this.state.email)],
              ["pass", JSON.stringify(this.state.pass)],
            ], () => {
              this.setState({ loading: false }, () => {
                this.props.navigation.dispatch(resetAction("TabsRoute"))
              })
            })
            .catch(error => {
              this.setState({ loading: false })
              alert(`Enable App access to SD: ${error}`)
            })
          } else {
            this.setState({ loading: false })
            alert(res.error)
          }
        })
        .catch(error => {
          alert(`Check your WiFi/Cellular Data: ${error}`)
          this.setState({ loading: false })
        })
    } else {
      this.setState({ submitted: true, loading: false })
    }
  }
  validateEmail = email => {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return re.test(email)
  }
  onLayout = event => {
    const { width, height } = event.nativeEvent.layout
    const landscape = width > height ? true : false
    this.setState({ landscape })
  }
  render() {
    return(
      <TouchableWithoutFeedback
        onLayout={this.onLayout}
        onPress={dismissKeyboard}
      >
        <View style={{ flex: 1 }}>
          <View style={{
            backgroundColor: Colors.Primary,
            display: this.state.landscape ? "none" : "flex",
          }}>
            <Entypo
              style={{ textAlign: "center", margin: 50 }}
              name="user"
              size={50}
              color={Colors.Content}
            />
          </View>
          <View style={{ 
            flex: 1,
            borderTopWidth: 1,
            borderTopColor: Colors.Secondary,
            display: this.state.loading ? "flex" : "none"
          }}>
            <ActivityIndicator animating size="large" style={{ padding: 50 }}/>
            <Text style={{ textAlign: "center" }}>Checking credentials, plese wait...</Text>
          </View>
          <View style={{ display: this.state.loading ? "none" : "flex" }}>
            <TextInput
              underlineColorAndroid={Colors.primary}
              placeholder="Email"
              placeholderTextColor={Colors.Primary}
              keyboardType="email-address"
              selectionColor={Colors.Primary}
              style={Styles.input}
              autoCapitalize="none"
              autoCorrect={false}
              onChangeText={email => this.setState({email})}
              returnKeyType="next"
              value={this.state.email}
              onSubmitEditing={() => this.passwordRef.focus()}
            />
            <FormValidationMessage  style={{ height: 20 }}>
              {this.state.submitted && this.state.email === "" ? "Email field is required" : this.state.submitted && !this.validateEmail(this.state.email) ? "Please valid email is required" : null}
            </FormValidationMessage>
            <TextInput
              ref={ref => this.passwordRef = ref}
              underlineColorAndroid={Colors.primary}
              placeholder="Password"
              placeholderTextColor={Colors.Primary}
              secureTextEntry={true}
              selectionColor={Colors.Primary}
              style={Styles.input}
              autoCapitalize="none"
              autoCorrect={false}
              onChangeText={pass => this.setState({pass})}
              returnKeyType={this.state.isSignup ? "next" : "go"}
              value={this.state.pass}
              onSubmitEditing={this.handleSubmit}
            />
            <FormValidationMessage style={{ height: 20 }}>
              {this.state.submitted && this.state.pass === "" ? "Password field is required" : null}
            </FormValidationMessage>
            <Button
              raised
              style={Styles.loginButton}
              backgroundColor={Colors.Primary}
              borderRadius={3}
              large
              iconRight
              icon={{name: "code"}}
              title="Login"
              onPress={this.handleSubmit}
            />
          </View>
        </View>
      </TouchableWithoutFeedback>
    )
  }
}

export default Login