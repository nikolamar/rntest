// @flow weak

import React, { Component } from 'react'
import { View, FlatList, AsyncStorage } from 'react-native'
import { List, ListItem } from 'react-native-elements'
import RenderSeparator from '../Views/RenderSeparator'
import Search from '../Views/Search'
import Footer from '../Views/Footer'
import Styles from '../Styles'

class EmployeesScreen extends Component {
  state = {
    page: 1,
    data: [],
    loading: false,
    refreshing: false
  }
  componentWillMount() {
    this.fetchEmployees()
  }
  handleRefreshing = () => {
    this.setState({
      page: 1,
      refreshing: true,
    },
    () => {
      this.fetchEmployees()
    })
  }
  handleEndOfList = () => {
    this.setState({
      page: this.state.page + 1
    },
    () => {
      this.fetchEmployees()
    })
  }
  fetchEmployees = async () => {
    const { page } = this.state
    const token = await AsyncStorage.getItem("token")
    const url = `https://test.inploi.me/jobs/${page}?token=${token.replace(/"/g, "")}`
    this.setState({ loading: true })

    fetch(url)
      .then(res => res.json())
      .then(res => {
        this.setState({
          data: page === 1 ? res.browse : [...this.state.data, ...res.browse],
          loading: false,
          refreshing: false
        })
      })
      .catch(err => {
        this.setState({ err, loading: false, refreshing: false })
      })
  }
  employeeSelected = item => {
    this.props.navigation.navigate("EmployeeDetails", item)
  }
  render() {
    return (
      <List containerStyle={{ flex: 1, marginTop: 0, borderBottomWidth: 0, borderTopWidth: 0 }}>
        <FlatList
          data={this.state.data}
          renderItem={({ item }, i) => (
            <ListItem
              roundAvatar
              title={item.first_name}
              subtitle={item.role}
              avatar={{uri: `https://res.cloudinary.com/chris-mackie/image/upload/c_thumb/v1/${item.employer_img}`}}
              containerStyle={{ borderBottomWidth: 0 }}
              onPress={() => this.employeeSelected(item)}
            />
          )}
          keyExtractor={item => item.id}
          ItemSeparatorComponent={RenderSeparator}
          ListHeaderComponent={Search}
          ListFooterComponent={() => <Footer loading={this.state.loading}/>}
          refreshing={this.state.refreshing}
          onRefresh={this.handleRefreshing}
          onEndReached={this.handleEndOfList}
          onEndTreshold={20}
        />
      </List>
    )
  }
}

export default EmployeesScreen