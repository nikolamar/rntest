// @flow weak

import React, { Component } from 'react'
import { ScrollView, View, Text } from 'react-native'
import { Tile, List, ListItem } from 'react-native-elements'
import EmployeeModel from '../Models/EmployeeModel'
import { Colors } from '../Styles'

function humanize(str) {
  var frags = str.split('_')
  for (i=0; i<frags.length; i++) {
    frags[i] = frags[i].charAt(0).toUpperCase() + frags[i].slice(1)
  }
  return frags.join(' ')
}

class EmployeeDetails extends Component {
  render() {
    if (!this.props.navigation.state.params) {
      return (
        <View style={{
          flex:1,
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "center"
        }}>
          <Text style={{ fontSize: 20, color: Colors.Primary }}>Please select employee</Text>
        </View>
      )
    }
    const employee = new EmployeeModel(this.props.navigation.state.params)
    return (
      <ScrollView>
        <Tile
          featured
          imageSrc={{ uri: `https://res.cloudinary.com/chris-mackie/image/upload/c_thumb/v1/${employee.employer_img}` }}
          title={employee.first_name}
          caption={employee.role}
        />
        <List containerStyle={{ flex: 1, marginTop: 0, borderBottomWidth: 0, borderTopWidth: 0 }}>
          {Object.keys(employee).map((key, i) => {
            const prop = employee[key]
            // Don't have time for this so speeding up
            if (prop && typeof prop === "string") {
              return (
                <ListItem
                  key={i}
                  title={humanize(key)}
                  rightTitle={prop}
                  hideChevron
                />
              )
            } else {
              return null
            }
          })}
        </List>
      </ScrollView>
    )
  }
}

export default EmployeeDetails