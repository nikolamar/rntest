// @flow weak

import headerConfig from '../Configs/headerConfig'
import Login from '../Screens/Login'

const LoginRoute = {
  screen: Login,
  navigationOptions: {
    ...headerConfig
  }
}

export default LoginRoute