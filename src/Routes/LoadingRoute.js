// @flow weak

import Loading from '../Screens/Loading'
import headerConfig from '../Configs/headerConfig'

const LoadingRoute = {
  screen: Loading,
  navigationOptions: {
    ...headerConfig
  }
}


export default LoadingRoute