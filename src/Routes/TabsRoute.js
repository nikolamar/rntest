// @flow weak

import { TabNavigator } from 'react-navigation'
import tabsConfig from '../Configs/tabsConfig'
import headerConfig from '../Configs/headerConfig'
import EmployeesList from '../Screens/EmployeesList'
import EmployeeDetails from '../Screens/EmployeeDetails'
import employeeIcon from '../Icons/employeeIcon'
import employeesListIcon from '../Icons/employeesListIcon'
import logoutIcon from '../Icons/logoutIcon'

const TabsRoute = { 
  screen: TabNavigator({
    EmployeesList: {
      screen: EmployeesList,
      navigationOptions: ({ navigation }) => ({
        title: "Employees List",
        ...headerConfig,
        ...employeesListIcon,
        ...logoutIcon(navigation)
      })
    },
    EmployeeDetails: {
      screen: EmployeeDetails,
      navigationOptions: ({ navigation }) => ({
        title: "Employee Details",
        ...headerConfig,
        ...employeeIcon,
        ...logoutIcon(navigation)
      })
    }
  },
  tabsConfig
)}

export default TabsRoute