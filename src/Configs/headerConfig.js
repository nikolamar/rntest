// @flow weak  

import { Colors } from '../Styles'

const headerConfig = {
  headerStyle: {
    backgroundColor: Colors.Primary
  },
  headerTitleStyle:{
    color: Colors.Content
  }
}

export default headerConfig