// @flow weak

import { Colors } from '../Styles'

const tabsConfig = {
  swipeEnabled: true,
  animationEnabled: true,
  tabBarOptions: {
    inactiveTintColor: Colors.Secondary,
    activeTintColor: Colors.Selected,
    labelStyle: {
      fontSize: 12,
    },
    style: {
      backgroundColor: Colors.Primary
    }
  }
}

export default tabsConfig