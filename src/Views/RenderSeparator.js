// @flow weak

import React from 'react'
import { View } from 'react-native'
import { Colors } from '../Styles'

const RenderSeparator = () => (
  <View
    style={{
      height: 1,
      width: "86%",
      backgroundColor: Colors.Secondary,
      marginLeft: "14%"
    }}
  />
)

export default RenderSeparator