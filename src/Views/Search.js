// @flow weak

import React from 'react'
import { SearchBar } from 'react-native-elements'

const Search = () => (
  <SearchBar placeholder="Search employees..." lightTheme round />
)

export default Search