// @flow weak

import React from 'react'
import { View, ActivityIndicator } from 'react-native'
import { Colors } from '../Styles'

const Footer = ({ loading }) => loading ? (
  <View style={{ flex: 1, paddingVertical: 20, borderTopWidth: 1, borderTopColor: Colors.Secondary }}>
    <ActivityIndicator animating size="large"/>
  </View>
) : null

export default Footer