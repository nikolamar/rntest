// @flow weak

import React from 'react'
import RootNavigator from './src/RootNavigator'

console.disableYellowBox = true

const App = () => (
  <RootNavigator/>
)

export default App
