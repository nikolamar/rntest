// @flow weak

import { StackNavigator } from 'react-navigation'
import TabsRoute from './Routes/TabsRoute'
import LoginRoute from './Routes/LoginRoute'
import LoadingRoute from './Routes/LoadingRoute'

const RootNavigator = StackNavigator({
  LoadingRoute,
  LoginRoute,
  TabsRoute
})

export default RootNavigator